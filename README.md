# devops-netology
Test commit. 1-12-21
С помощью файла .gitignore в директории \terraform будут проигнорированы следующие файлы:
- из директории **/.terraform/*
- файлы с шаблонами *.tfstate и *.tfstate.*
- файл crash.log
- все файлы с расширением *.tfvars
- файлы override.tf
	override.tf.json
	*_override.tf
	*_override.tf.json
- файлы .terraformrc
	terraform.rc